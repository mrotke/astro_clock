/*
 * EkranLCD8x2.cpp
 *
 *  Created on: Mar 4, 2018
 *      Author: michal
 */

#include "../../../src/DEV/LCD8x2/EkranLCD8x2.h"

EkranLCD8x2::EkranLCD8x2(uint8_t adr):
LiquidCrystal_PCF8574(adr)
{
	// TODO Auto-generated constructor stub

}

void EkranLCD8x2::clear()
{
	LiquidCrystal_PCF8574::clear();
	this->col_num = 0;
}

void EkranLCD8x2::setCursor(uint8_t col)
{
	uint8_t row = (col/8);
	col = col % 8;
	LiquidCrystal_PCF8574::setCursor(col, row);
	this->col_num = (col + (row * 8));
}

void EkranLCD8x2::home() {
	LiquidCrystal_PCF8574::home();
	this->col_num = 0;
}

inline size_t EkranLCD8x2::write(uint8_t value) {
	LiquidCrystal_PCF8574::write(value);
	col_num++;
	if (col_num == 8)
		LiquidCrystal_PCF8574::setCursor(0, 1);
	return 1;

}

EkranLCD8x2::~EkranLCD8x2() {
	// TODO Auto-generated destructor stub
}


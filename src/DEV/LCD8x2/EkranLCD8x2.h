/*
 * EkranLCD8x2.h
 *
 *  Created on: Mar 4, 2018
 *      Author: michal
 */

#ifndef SRC_DEV_EKRANLCD8X2_H_
#define SRC_DEV_EKRANLCD8X2_H_

#include <LiquidCrystal_PCF8574.h>

class EkranLCD8x2: public LiquidCrystal_PCF8574 {
public:
	EkranLCD8x2(uint8_t adr);
	void begin(){LiquidCrystal_PCF8574::begin(8, 2);};
	void clear();
	void setCursor(uint8_t col);
	void home();
	virtual size_t write(uint8_t value);
	virtual ~EkranLCD8x2();
private:
	uint8_t col_num =0;
};

#endif /* SRC_DEV_EKRANLCD8X2_H_ */

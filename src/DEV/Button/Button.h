/*
 * Button.h
 *
 *  Created on: Mar 4, 2018
 *      Author: michal
 */


#ifndef SRC_DEV_BUTTON_BUTTON_H_
#define SRC_DEV_BUTTON_BUTTON_H_


#include "Arduino.h"

enum  switch_state{
	IDLE,
	PUSH,
	WAIT,
	REPEAT
};

class Button{
typedef void( * callback)(void);

public:
	Button(uint8_t pin,uint8_t push_wait,uint8_t repeat_wait,int8_t repeat_time,callback pushFunction,callback repeatFunction = 0);
	static void debounce();
	virtual ~Button();
	static Button * table[2]; // maksymalnie 2 przyciskow
	static uint8_t index;
	uint8_t state = IDLE;
	uint8_t timer=123;
	uint8_t pin;
private:
	callback pushFunction;
	callback repeatFunction = 0;



	uint8_t push_wait;
	uint8_t repeat_wait;
	uint8_t repeat_time;
};

#endif /* SRC_DEV_BUTTON_BUTTON_H_ */

/*
 * Button.cpp
 *
 *  Created on: Mar 4, 2018
 *      Author: michal
 */

#include "../../../src/DEV/Button/Button.h"

uint8_t Button::index=0;
Button * Button::table[2];

Button::Button(uint8_t pin,uint8_t push_wait,uint8_t repeat_wait,int8_t repeat_time,callback pushFunction,callback repeatFunction):
pin(pin),
push_wait(push_wait),
repeat_wait(repeat_wait),
repeat_time(repeat_time),
pushFunction(pushFunction),
repeatFunction(repeatFunction),
state(IDLE)
{
	pinMode(pin, INPUT_PULLUP);
	table[index] = this;
	index++;

}

void Button::debounce()
{
	for (uint8_t i=0;i<index;i++)
	{
		(table[i]->timer)++;
		switch ((table[i]->state)){
			case IDLE:
				if (!(digitalRead(table[i]->pin))) {
					table[i]->state = PUSH;
					table[i]->timer = 0;
				}
				break;
			case PUSH:
				if ((table[i]->timer) >= (table[i]->push_wait)) {
					if (!(digitalRead(table[i]->pin))) {
						table[i]->timer = 0;
						if (table[i]->pushFunction)
							table[i]->pushFunction();
						table[i]->state = WAIT;
					} else
						table[i]->state = IDLE;
				}
				break;
			case WAIT:
				if ((table[i]->timer) >= (table[i]->repeat_wait)) {
					if (!(digitalRead(table[i]->pin))) {
						table[i]->timer = 0;
						if (table[i]->repeatFunction)
							table[i]->repeatFunction();
						table[i]->state = REPEAT;
					} else
						table[i]->state = IDLE;
				}

				break;
			case REPEAT:
				if ((table[i]->timer) >= (table[i]->repeat_time)) {
					if (!(digitalRead(table[i]->pin))) {
						table[i]->timer = 0;
						if (table[i]->repeatFunction)
							table[i]->repeatFunction();
						table[i]->state = REPEAT;
					} else
						table[i]->state = IDLE;
				}

				break;

	}
}

}

Button::~Button() {
	// TODO Auto-generated destructor stub
}



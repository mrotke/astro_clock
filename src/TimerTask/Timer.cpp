/*
 * Timer.cpp
 *
 *  Created on: 21 sty 2018
 *      Author: michal
 */

#include "../../src/TimerTask/Timer.h"

uint8_t Timer::index=0;
Timer * Timer::table[5];

Timer::Timer() {
	// TODO Auto-generated constructor stub
	table[index]= this;
	index++;

}

void Timer::every(callback fun, uint16_t time) {
	pFunction = fun;
	timerOut = time;
	startTime = millis();
}

void Timer::update() {
	for (uint8_t i=0;i<index;i++)
	{
		if ((millis()- table[i]->startTime)>= table[i]->timerOut)
			{
				if (table[i]->pFunction)
					table[i]->pFunction();
				table[i]->startTime = millis();
			}
	}
	sleep();
}

Timer::~Timer() {
	// TODO Auto-generated destructor stub
}


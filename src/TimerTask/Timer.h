/*
 * Timer.h
 *
 *  Created on: 21 sty 2018
 *      Author: michal
 */

#ifndef SRC_TIMERTASK_TIMER_H_
#define SRC_TIMERTASK_TIMER_H_
#include "Arduino.h"

class Timer {
private:
	typedef void( * callback)(void);
	callback pFunction=0;
	unsigned long timerOut=0;
	unsigned long startTime = 0;
	static Timer * table[5]; // maksymalnie 5 taskow
	static uint8_t index;

public:
	Timer();
	void every(callback fun,uint16_t time);
	static void update();
	virtual ~Timer();
};

#endif /* SRC_TIMERTASK_TIMER_H_ */

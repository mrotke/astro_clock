/*
 * LCDMenu.cpp
 *
 *  Created on: Mar 7, 2018
 *      Author: michal
 */

#include <src/Objects/MenuSystem/LCDMenu.h>


LCDMenu* LCDMenu::instance;
Menu*     LCDMenu::menu;

LCDMenu* LCDMenu::getInstance() {
	if (instance == 0)
		instance = new LCDMenu();
	return instance;
}

void LCDMenu::DisplayMenu(char* buf) {
	strcpy_P(buf, (char*)pgm_read_word(&(string_table[menu->getCurrentIndex()])));
}

void LCDMenu::initMenu() {
	menu = new Menu(menuChanged,menuUsed);
	MenuItem zegar = MenuItem();
	menu->addMenuItem(zegar);

	MenuItem data = MenuItem();
	menu->addMenuItem(data);

	MenuItem alarm1 = MenuItem();
	menu->addMenuItem(alarm1);

	MenuItem alarm2 = MenuItem();
	menu->addMenuItem(alarm2);
}

Menu* LCDMenu::getMenu() {
	return menu;
}

LCDMenu::LCDMenu()
{
	initMenu();
}

void LCDMenu::menuChanged(ItemChangeEvent event)
{
}

void LCDMenu::menuUsed(ItemUseEvent event)
{
}

LCDMenu::~LCDMenu() {
	// TODO Auto-generated destructor stub
}


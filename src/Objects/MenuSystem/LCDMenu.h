/*
 * LCDMenu.h
 *
 *  Created on: Mar 7, 2018
 *      Author: michal
 */

#ifndef SRC_OBJECTS_MENUSYSTEM_LCDMENU_H_
#define SRC_OBJECTS_MENUSYSTEM_LCDMENU_H_

#include <src/Objects/MenuSystem/MenuLibrary/Menu.h>
#include <src/Objects/MenuSystem/MenuLibrary/MenuItem.h>

#include <avr/pgmspace.h>

const char string_0[] PROGMEM = "Zegar";
const char string_1[] PROGMEM = "Data ";
const char string_2[] PROGMEM = "Alarm1";
const char string_3[] PROGMEM = "Alarm2";

const char* const string_table[] PROGMEM = {
		string_0,
		string_1,
		string_2,
		string_3
};


class LCDMenu{
public:
	virtual ~LCDMenu();
	static LCDMenu* getInstance();
	Menu* getMenu();
	static void DisplayMenu(char*);
private:
	static void menuChanged(ItemChangeEvent event);
	void initMenu();
	static void menuUsed(ItemUseEvent event);
	static LCDMenu *instance;
	LCDMenu();
	static Menu* menu;

};

#endif /* SRC_OBJECTS_MENUSYSTEM_LCDMENU_H_ */

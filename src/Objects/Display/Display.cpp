/*
 * Display.cpp
 *
 *  Created on: Mar 7, 2018
 *      Author: michal
 */

#include <src/Objects/Display/Display.h>

Display* Display::instance;

Display* Display::getInstance() {
	if(!instance)
		instance = new Display();
	return instance;
}
void Display::wyswietl(){
	getInstance()->refresh();
}

void Display::refresh() {
	if (fun)
	{
		fun(bufor);
		display->home();
		for (uint8_t i=0; i< sizeof(bufor);i++)
		{
			if (*(bufor+i))
				display->print(*(bufor+i));
		}


	}
}

void Display::registerCallback(callback fun)
{
	this->fun =fun;
}

Display::Display() {
 display = new EkranLCD8x2(0x27);
}

Display::~Display() {
	// TODO Auto-generated destructor stub
}

EkranLCD8x2* Display::getLCD() {
	return display;

}

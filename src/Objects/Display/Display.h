/*
 * Display.h
 *
 *  Created on: Mar 7, 2018
 *      Author: michal
 */

#ifndef SRC_OBJECTS_DISPLAY_DISPLAY_H_
#define SRC_OBJECTS_DISPLAY_DISPLAY_H_

#include "src/DEV/LCD8x2/EkranLCD8x2.h"


class Display {
typedef void( * callback)(char*);

public:
	static Display* getInstance();
	void refresh();
	void registerCallback(callback fun);
	static void wyswietl();
	EkranLCD8x2 *getLCD();
	virtual ~Display();
private:
	Display();
	EkranLCD8x2 *display;
	static Display *instance;
	char bufor[16] = {' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '};
	callback fun;
};

#endif /* SRC_OBJECTS_DISPLAY_DISPLAY_H_ */

/*!
 * @author Michal Rotkegel
 */
#include "Arduino.h"

#include "Wire.h"

#include "src/DEV/RTC/DS3231.h"
#include "src/TimerTask/Timer.h"
#include "src/DEV/Button/Button.h"
#include "src/Objects/Display/Display.h"
#include "src/Objects/MenuSystem/LCDMenu.h"


Button button1 = Button(18, 5, 1000, 100, button1_fun, button1_fun);
Button button2 = Button(19, 5, 1000, 100, button2_fun, button2_fun);
Timer button_Timer = Timer();
Timer Seconds = Timer();
uint8_t test =0;

/// the setup function runs once when you press reset or power the board
void setup() {
  pinMode(24, OUTPUT);
  Wire.begin();
  Display::getInstance()->getLCD()->begin();
  button_Timer.every(Button::debounce, 1);
  Seconds.every(Display::wyswietl,30);
  Display::getInstance()->getLCD()->setBacklight(255);
  Display::getInstance()->registerCallback(LCDMenu::getInstance()->DisplayMenu);
  sleepMode(SLEEP_IDLE);




}

/// the loop function runs over and over again forever
void loop() {
  Timer::update();
}



void button1_fun()
{
	LCDMenu::getInstance()->getMenu()->up();
}

void button2_fun()
{
	LCDMenu::getInstance()->getMenu()->down();
}

